const discord = require("discord.js");
const client = new discord.Client();

class DiscordSocket {
  constructor(Token) {
    client.login(Token);
    return client;
  }
}

module.exports = DiscordSocket;
