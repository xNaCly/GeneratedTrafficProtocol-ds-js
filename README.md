# GeneratedTrafficProtocol-ds.js
GTP => discord wrapper

discord-api-wrapper for the cryars project: [Cryars](https://github.com/Flam3rboy/cryars)

#### Authentication:
	new qintcord([token])
#### Logging in:
	.gateway_connect()
	
	connects the bot to the websocket
#### Sends a message:
	.send_message([channel_id],[message]) 
	
	both parameters need to be stringtype
	
	sends a message to the specified channel id
#### All guilds:
	.get_guilds()
	
	returns all guilds
#### Find guild by name:
	.get_guild_id_by_name([guild_name])
	
	parameter needs to be stringtpye
	
	returns the guild object for the specified name
# missing methods will be added soon ^^
